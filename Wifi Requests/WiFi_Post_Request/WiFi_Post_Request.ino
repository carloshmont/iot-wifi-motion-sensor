//=================================================================================================================
// Title:   WIFI + PIR + DHT Sensors
// Purpose: Demonstration
// Author:  Created by Carlos H. Montenegro
// Team:    IoT Squad
//
// Comments: 
// Use as base for the future WIFI library, GET and POST methods implemented for ESP8266 Module
//=================================================================================================================

#include <DHT.h>
#include <WiFiEsp.h>
#include <SoftTimer.h>

#define IDLE_CONNECTION_LED 8
#define SUCCESSFUL_CONNECTION_LED 9
#define CONNECTION_ERROR_LED 10

#define SW_SERIAL_RX 3
#define SW_SERIAL_TX 4

#define PIR_INTERRUPT 0
#define SERVER_PORT 80

#define DHTPIN 7     // what pin we're connected to
#define DHTTYPE DHT22   // DHT 22  (AM2302)

#define TIMER_PERIOD 1 //Minutes for callback 

DHT dht(DHTPIN, DHTTYPE);

#ifndef HAVE_HWSERIAL1
  #include <NeoSWSerial.h>
  NeoSWSerial WiFiSerial(SW_SERIAL_RX, SW_SERIAL_TX); //RX, TX
#endif

//MARK: - Web Client setup variables

char ssid[] = "TKIOT";
char pass[] = "";
char server[] = "team-iot.herokuapp.com";
int status = WL_IDLE_STATUS;
WiFiEspClient client;

//MARK: - Timer setup variables
SoftTimer timer;

void setup() {

  pinMode(IDLE_CONNECTION_LED, OUTPUT);
  pinMode(SUCCESSFUL_CONNECTION_LED, OUTPUT);
  pinMode(CONNECTION_ERROR_LED, OUTPUT);
  
  Serial.begin(115200);
  WiFiSerial.begin(9600);
  WiFi.init(&WiFiSerial);

  dht.begin();
  // WIFI Shield setup

  if(WiFi.status() == WL_NO_SHIELD) {
    Serial.println(F("WiFi shield not present"));
    setErrorLed();
    while(true);
  }

  while(status != WL_CONNECTED) {
    Serial.print(F("Attempting to connect to WPA SSID: "));
    Serial.println(ssid);
    // Trying to connect to WPA/WPA2 network
    status = WiFi.begin(ssid, pass);

    if(status == WL_CONNECT_FAILED) {
      setErrorLed();
    }
  }
  
  Serial.println(F("You're connected to the network"));
  printWifiStatus();
  
  // Motion Sensor setup
  attachInterrupt(PIR_INTERRUPT, intruder_detect, RISING);

  // Timer setup
  timer.schedule(TIMER_PERIOD*60000, sendTemperatureAndHumidityData);
}

void loop() { // Used for serial monitor debugging
//  while(client.connected()) {
//    if(client.available()){
//      char str = client.read();
//      Serial.println(str);
//    }
//  }
  timer.update();
}

void intruder_detect() { // This function is called whenever an intruder is detected by the arduino
  Serial.println(F("Intruder detected, send alert data to server!!"));
  triggerPostRequestForValue(); 
}

boolean sendTemperatureAndHumidityData(EventBase* evt) {
  detachInterrupt(PIR_INTERRUPT);

  float h = dht.readHumidity();
  float t = dht.readTemperature();
  
  if (client.connect(server, SERVER_PORT)) {
    Serial.println(F("Sending climate data to server..."));
    Serial.println(F("Connection to server OK!"));
    setConnectedLed();
    // If successful, start the call
    String postData = "{\"measurement\": {\"temperature\": \"" + String(t) + "\", \"humidity\": \"" + String(h) + "\", \"sensor\": \"Tekton Labs\" }}";
    client.println(F("POST /measurements.json HTTP/1.1"));
    client.println(F("Host: team-iot.herokuapp.com"));
    client.println("Content-Length: " + String(postData.length()));
    client.println(F("Content-Type: application/json"));
    client.println();
    client.println(postData);
    client.stop();
    setIdleLed();
  }
  attachInterrupt(PIR_INTERRUPT, intruder_detect, RISING);
  return false;
}

void triggerPostRequestForValue() {
  detachInterrupt(PIR_INTERRUPT);
  if (client.connect(server, SERVER_PORT)) {
    Serial.println(F("Connection to server OK!"));
    setConnectedLed();
    // If successful, start the call
    String postData = "{\"movement\": {\"value\": \"" + String(1) + "\",\"sensor\": \"Alderaan Sensor\" }}";
    client.println(F("POST /movements.json HTTP/1.1"));
    client.println(F("Host: team-iot.herokuapp.com"));
    client.println("Content-Length: " + String(postData.length()));
    client.println(F("Content-Type: application/json"));
    client.println();
    client.println(postData);
    client.stop();
    setIdleLed();
  }
  attachInterrupt(PIR_INTERRUPT, intruder_detect, RISING);
}

//MARK: - Interfacing Methods

void setConnectedLed() {
  digitalWrite(SUCCESSFUL_CONNECTION_LED, HIGH);
  digitalWrite(IDLE_CONNECTION_LED, LOW);
  digitalWrite(CONNECTION_ERROR_LED, LOW);
}

void setErrorLed() {
  digitalWrite(SUCCESSFUL_CONNECTION_LED, LOW);
  digitalWrite(IDLE_CONNECTION_LED, LOW);
  digitalWrite(CONNECTION_ERROR_LED, HIGH);
}

void setIdleLed() {
  digitalWrite(SUCCESSFUL_CONNECTION_LED, LOW);
  digitalWrite(IDLE_CONNECTION_LED, HIGH);
  digitalWrite(CONNECTION_ERROR_LED, LOW);
}

void printWifiStatus()
{
  setIdleLed();
  // print the SSID of the network you're attached to
  Serial.print(F("SSID: "));
  Serial.println(WiFi.SSID());

  // print your WiFi shield's IP address
  IPAddress ip = WiFi.localIP();
  Serial.print(F("IP Address: "));
  Serial.println(ip);

  // print the received signal strength
  long rssi = WiFi.RSSI();
  Serial.print(F("Signal strength (RSSI):"));
  Serial.print(rssi);
  Serial.println(F(" dBm"));
}
